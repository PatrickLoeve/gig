# frozen_string_literal: true

require_relative "gig/version"
require "gig/cli"

module Gig
  def self.call(options)
    cli = CLI.new(options)
    cli.grep
  end
end
