# frozen_string_literal: true

require "net/http"
require "json"
require "colorize"
require "open-uri"

module Gig
  class CLI
    attr_reader :options

    def initialize(options)
      @options = options
    end

    def grep
      create_folder
      uri = URI("https://api.github.com/search/repositories?q=#{options.join("+")}")
      res = Net::HTTP.get_response(uri)
      parsed_response = JSON.parse(res.body)

      items = parsed_response["items"]
      items.each do |item|
        avatar = item["owner"]["avatar_url"]
        save_avatar(avatar)
      end
    end

    private

    def create_folder
      return if options.empty?

      directory = folder_name
      Dir.mkdir directory unless Dir.exist?(directory)
    end

    def save_avatar(item)
      uri = URI(item)
      response = Net::HTTP.get_response(uri)

      directory = folder_name
      extension = "." + content_type_image(response)
      avatar_name = item.to_s.split("/")[-1].split("?")[0] + extension
      return if File.exist?("#{directory}/#{avatar_name}")

      File.write("#{directory}/#{avatar_name}", response.body)
      puts "* #{avatar_name} downloaded successfully!".colorize(:green)
    end

    def content_type_image(response)
      response.content_type.split("/")[-1]
    end

    def folder_name
      options.join("-")
    end
  end
end
