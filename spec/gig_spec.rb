# frozen_string_literal: true

RSpec.describe Gig do
  it "has a version number" do
    expect(Gig::VERSION).not_to be nil
  end

  subject { described_class.call(options) }

  let(:options) { ["topic:rails", "topic:ruby"] }
  let(:folder_name) { "topic:rails-topic:ruby" }

  it "creates a folder" do
    subject
    expect(Dir).to exist(folder_name)
  end

  describe "the search_url" do
    it "queries ruby repositories on GitHub" do
      uri = URI("https://api.github.com/search/repositories?q=topic:ruby")
      response = Net::HTTP.get(uri)
      expect(response).to be_an_instance_of(String)
    end
  end

  it "puts some text after downloading the avatar" do
    expect { Gig.call(["topic:ruby", "topic:rails"]) }
      .to output(a_string_including("downloaded successfully!")).to_stdout
  end
end
